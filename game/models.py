from django.db import models


class Game(models.Model):
    name = models.CharField(max_length=255, null=True, blank=True)
    rank = models.IntegerField(null=True, blank=True)
    platform = models.CharField(max_length=255, null=True, blank=True)
    year_released = models.IntegerField(null=True, blank=True)
    category = models.CharField(max_length=255, null=True, blank=True)
    producer = models.CharField(max_length=255, null=True, blank=True)
    usa_sale = models.IntegerField(null=True, blank=True)
    eu_sale = models.IntegerField(null=True, blank=True)
    japan_sale = models.IntegerField(null=True, blank=True)
    global_sale = models.IntegerField(null=True, blank=True)
