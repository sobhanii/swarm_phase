import json

from django.shortcuts import render

from rest_framework import authentication
from rest_framework.decorators import api_view
from rest_framework.response import Response


# Create your views here.
from game.models import Game


class CsrfExemptSessionAuthentication(authentication.SessionAuthentication):
    def enforce_csrf(self, request):
        return


@api_view(['GET'])
def rank_info(request):
    request = json.load(request)
    rank = request['rank']
    games = Game.objects.filter(rank=rank)
    return Response(list(games.values('name')))


@api_view(['GET'])
def name_info(request):
    request = json.load(request)
    name = request['name']
    games = Game.objects.filter(name=name)
    return Response(list(games.values('name','platform','category')), status=200)


@api_view(['GET'])
def best_platform(request):
    request = json.load(request)
    count = request['n']
    count = int(count)
    platform = request['platform']
    games = Game.objects.filter(platform=platform).order_by('global_sale')[:count]
    return Response(list(games.values('name', 'platform', 'global_sale')), status=200)


@api_view(['GET'])
def best_year(request):
    request = json.load(request)
    count = request['n']
    count = int(count)
    category = request['category']
    games = Game.objects.filter(category=category).order_by('global_sale')[:count]
    return Response(list(games.values('name', 'platform', 'global_sale','year_released')), status=200)


@api_view(['GET'])
def best_category(request):
    request = json.load(request)
    count = request['n']
    count = int(count)
    year = request['year']
    year = int(year)
    games = Game.objects.filter(year_released=year).order_by('global_sale')[:count]
    return Response(list(games.values('name', 'platform', 'global_sale','category')), status=200)


@api_view(['GET'])
def best_year_and_plat(request):
    request = json.load(request)
    count = 5
    platform = request['platform']
    year = request['year']
    year = int(year)
    games = Game.objects.filter(platform=platform,year_released=year).order_by('global_sale')[:count]
    return Response(list(games.values('name', 'platform', 'global_sale','year_released')), status=200)

