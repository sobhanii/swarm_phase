"""final_swarm_sample URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from game import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/get_info_with_rank/', views.rank_info),
    path('api/get_info_with_name/', views.name_info),
    path('api/best_games_platform/', views.best_platform),
    path('api/best_games_year/', views.best_year),
    path('api/best_games_category/', views.best_category),
    path('api/best_games_year_platform/', views.best_year_and_plat),
    # path('api/more_eu_usa/', views.more_eu_usa())
]
